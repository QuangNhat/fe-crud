export class User {
    id: Number;
    name: String;
    email: String;
    birth: Date;
    phone: Number;
    schoolclass: String;
    gender: Boolean
    constructor (id,name, email,phone,birth,schoolclass, gender = false) {
        this.id = id;
        this.name= name;
        this.email= email;
        this.birth = birth
        this.phone = phone; 
        this.schoolclass = schoolclass;
        this.gender = gender;
    }
    private setName(name) {
        this.name = name;
    }
    private setEmail(email) {
        this.email = email;
    }
    private setBirth(birth) {
        this.birth = birth;
    }
    private setPhone(phone) {
        this.phone = phone;
    }
    private setSchoolClass(schoolclass) {
        this.schoolclass = schoolclass;
    }
    private setGender(gender) {
        this.gender = gender;
    }
}