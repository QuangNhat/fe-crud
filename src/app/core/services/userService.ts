import { Injectable } from "@angular/core";
import { User } from "../model/User";

@Injectable({
    providedIn: 'root'
})
export class UserService {
    getUsers() {
        return [
            new User(1, 'Lê Văn Tèo', 'teo123@gmail.com', '012332090', '1992-02-23', '10A', true),
            new User(12, 'Phan Thị Li', 'li321@gmail.com', '0809885', '1992-12-30', '12D', false),
            new User(3, 'Trần Văn Nam', 'namtran123@gmail.com', '0625347235', '1997-05-23', '10H', true),
            new User(14, 'Lê Đạt', 'dat345@gmail.com', '012332090', '1992-02-23', '11C', true),
            new User(5, 'Trần Thị Thu', 'thutran098@gmail.comT2', '0809885', '1992-12-30', '10B', false),
            new User(6, 'Hoàng Thị Hà', 'hahoang123@gmail.com', '0625347235', '1997-05-23', '12A', false),
        ]
    }
    createUser(user: any) {
        let newuser = new User(
            1,
            user.nameInput,
            user.emailInput,
            user.phoneInput,
            user.birthInput,
            user.schoolclassInput,
            user.genderEditInput
        )
        console.log(user);
    }
    deleteUser(user: User) {
        //destroy
    }
    updateUser(user: any) {
        let newuser = new User(
            1,
            user.nameInput,
            user.emailInput,
            user.phoneInput,
            user.birthInput,
            user.schoolclassInput,
            user.genderEditInput
        )
        console.log(user);
    }
}