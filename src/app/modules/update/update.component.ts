import { Component, OnInit, Input, ViewChild, Output, EventEmitter, OnDestroy, OnChanges } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { User } from "src/app/core/model/User";
import { Schoolclass } from "src/app/core/model/Schoolclass";
import { ModalDirective } from "angular-bootstrap-md";

@Component({
    selector: 'update-component',
    templateUrl: './update.component.html',
})

export class UpdateComponent implements OnInit {
    cl = Array();
    grade = Array();
    gender:boolean;
    constructor() {
        for (let i = 0; i < 3; i++) {
            this.grade.push(10 + i);
        }
    }
  
    ngOnInit() {
        this.initAddForm();
        console.log(this.student);
        this.getClassList(this.updateForm.value.gradeInput.substring(0,2));
    }
  
    getClassList(value: any) {
        this.cl = [];
        for (let i = 1; i <= 8; i++) {
            this.cl.push(new Schoolclass(i, value + String.fromCharCode(64 + i)));
        }
    }
    @ViewChild(ModalDirective) modal : ModalDirective
    updateForm: FormGroup;
    @Output() updateUserInstanceEvent: EventEmitter<any> = new EventEmitter<any>();
    @Input() student : User;
    @Input() schoolclass : Schoolclass;
    initAddForm() {

        this.updateForm = new FormGroup({
            idInput: new FormControl(
                this.student.id,
                [
                    Validators.required,
                ]
            ),
            nameInput: new FormControl(
                this.student.name,
                [
                    Validators.required,
                ]
            ),
            emailInput: new FormControl(
                this.student.email,
                [
                    Validators.email,
                    Validators.required,
                ]
            ),
            phoneInput: new FormControl(
                this.student.phone,
                [
                    Validators.minLength(5),
                    Validators.maxLength(20),
                    Validators.required,
                    Validators.pattern("^(0[1-9][0-9]*)$"),
                ]
            ),
            birthInput: new FormControl(
                this.student.birth,
                [
                    Validators.required,
                ]
            ),
            genderEditInput: new FormControl(
                this.student.gender+"",
                [
                    Validators.required,
                ]
            ),
            
            schoolclassInput: new FormControl(
                this.student.schoolclass,
                [
                    Validators.required,
                ]
            ),

            gradeInput: new FormControl(
                this.student.schoolclass.substring(0,2),
            )
        });
    }
    get input() {
        return this.updateForm;
    }
    
    onSubmit() {
        if (this.updateForm.valid) {
            //
            console.log(this.gender);
            var formdata = this.updateForm.value;
            
            //tat form
            this.modal.hide();
            
            this.updateUserInstanceEvent.emit(formdata);
            
           
        } else {
            this.validateAllFormFields(this.updateForm);
        }
    }

    validateAllFormFields(formGroup: FormGroup) {
        Object.keys(formGroup.controls).forEach(field => {
            const control = formGroup.get(field);
            if (control instanceof FormControl) {
                control.markAsTouched({ onlySelf: true });
            } else if (control instanceof FormGroup) {
                this.validateAllFormFields(control);
            }
        });
    }

    setValue(){
        console.log("do somtttt");
        this.updateForm.controls['nameInput'].setValue(this.student.name);
        this.updateForm.controls['emailInput'].setValue(this.student.email);
        this.updateForm.controls['phoneInput'].setValue(this.student.phone);
        this.updateForm.controls['birthInput'].setValue(this.student.birth);
        this.updateForm.controls['genderEditInput'].setValue(this.student.gender+"");
       this.updateForm.controls['gradeInput'].setValue(this.student.schoolclass.substring(0,2));
       this.getClassList(this.student.schoolclass.substring(0,2));
       this.updateForm.controls['schoolclassInput'].setValue(this.student.schoolclass);
    }
}