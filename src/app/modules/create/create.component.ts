import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef } from "@angular/core";
import { Schoolclass } from "src/app/core/model/Schoolclass";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { ModalDirective } from "angular-bootstrap-md";


@Component({
    selector: 'create-component',
    templateUrl: './create.component.html',
})

export class CreateComponent implements OnInit {
    cl = Array();
    grade = Array();
    constructor() {
        for (let i = 0; i < 3; i++) {
            this.grade.push(10 + i);
        }
        this.getClassList(10);
    }
    ngOnInit() {
        this.initAddForm();
    }
  
    @Input() title: any;
    @Output() addUserInstanceEvent: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild(ModalDirective) modal: ModalDirective;
   
    addForm: FormGroup;

    initAddForm() {
        this.addForm = new FormGroup({
            nameInput: new FormControl(
                "",
                [
                    Validators.required,
                ]
            ),
            emailInput: new FormControl(
                "",
                [
                    Validators.email,
                    Validators.required,
                ]
            ),
            phoneInput: new FormControl(
                "",
                [
                    Validators.minLength(5),
                    Validators.maxLength(20),
                    Validators.required,
                    Validators.pattern("^(0[1-9][0-9]*)$"),
                ]
            ),
            birthInput: new FormControl(
                "",
                [
                    Validators.required,
                ]
            ),
            genderInput: new FormControl(
                "false",
                [
                    Validators.required,
                ]
            ),
            schoolclassInput: new FormControl(
                "10A",
                [
                    Validators.required,
                ]
            ),
        });
    }
    getClassList(value: any) {
        this.cl = [];
        for (let i = 1; i <= 8; i++) {
            this.cl.push(new Schoolclass(i, value + String.fromCharCode(64 + i)));
        }
    }



    onSubmit() {
        console.log(this.addForm.value);
        if (this.addForm.valid) {
            var formdata = this.addForm.value;
            //tat form
            this.modal.hide();
            this.addUserInstanceEvent.emit(formdata);
            this.addForm.reset();
        } else {
            this.validateAllFormFields(this.addForm);
        }
    }

    validateAllFormFields(formGroup: FormGroup) {
        Object.keys(formGroup.controls).forEach(field => {
            const control = formGroup.get(field);
            if (control instanceof FormControl) {
                control.markAsTouched({ onlySelf: true });
            } else if (control instanceof FormGroup) {
                this.validateAllFormFields(control);
            }
        });
    }
    get input() {
        return this.addForm;
    }
}
