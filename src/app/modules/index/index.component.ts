import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef, OnChanges } from '@angular/core';
import { User } from 'src/app/core/model/User';
import { FormGroup, FormControl } from '@angular/forms';
import { Schoolclass } from 'src/app/core/model/Schoolclass';
import { UserService } from 'src/app/core/services/userService';
import { UpdateComponent } from '../update/update.component';
import { CreateComponent } from '../create/create.component';


@Component({
    selector: 'index-component',
    templateUrl: './index.component.html',
    styleUrls: ['./index.component.scss']
})

export class IndexComponent implements OnInit {

    constructor(private userService : UserService) { 
        this.users = userService.getUsers();
    }
    ngOnInit() {
        this.initAddForm();
        this.initSchools();
    }
    @Input() value: any;
    @Output() deleteUserInstanceEvent: EventEmitter<any> = new EventEmitter<any>();
    formGroup : FormGroup;
    checkboxArray : any = [];
    
    initAddForm() {
        this.formGroup = new FormGroup ({
            checkInput : new FormControl(),
            checkAllInput : new FormControl(),
        });
    }

    handleDeleteClick(item: any) {
        // service
        this.userService.deleteUser(item);
        this.users.splice(this.users.indexOf(item), 1);
    }

    // flash() { console.log('flash')} 
    users = [];
    schools= [];

    initSchools() {
        var id =1;
        for (let j = 0; j < 3; j++) {
            var cl = 10+ j;
        for (let i = 1; i <= 8; i++ , id ++) {
            
            this.schools.push(new Schoolclass(id,  cl+ String.fromCharCode(64 + i)));
        }
    }
   
    }
    
    addUserEvent(user: any) {
        this.userService.createUser(user);
        this.users.push(new User(
                1,
                user.nameInput,
                user.emailInput,
                user.phoneInput,
                user.birthInput,
                user.schoolclassInput,
                user.genderEditInput
            ));
    }
    updateUserEvent(user: any) {
        var edituser;
        var filter  =this.users.filter((item, index) => {
            return item.id == user.idInput;
        });
        edituser = filter[0];
        edituser.setName(user.nameInput);
        edituser.setEmail(user.emailInput);
        edituser.setPhone(user.phoneInput);
        edituser.setBirth(user.birthInput);
        edituser.setGender(user.genderEditInput);
        edituser.setSchoolClass(user.schoolclassInput);
        console.log(edituser);
    }

    handleMassRemove() {
        this.checkboxArray.forEach(element => {
            var id = this.users.findIndex(x=> x.id == element);
            this.users.splice(id, 1);
        });
        this.checkboxArray= [];
    }

    handleCheckClick(id) {
        if (this.formGroup.value.checkInput) {
            if ( !this.checkboxArray.includes(id)){
            this.checkboxArray.push(id);
            }
        }else
        {
            this.checkboxArray.splice(this.checkboxArray.indexOf(id),1);
        }
    
        console.log(this.checkboxArray);
    }
   
    handleCheckAllClick(){
        this.formGroup.controls['checkInput'].setValue(this.formGroup.value.checkAllInput);
    }
  }