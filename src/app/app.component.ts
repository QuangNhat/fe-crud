import { Component, ViewChild } from '@angular/core';
import {ModalDirective} from 'angular-bootstrap-md';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  @ViewChild(ModalDirective) model: ModalDirective;
  title = 'app';
  
   
}
