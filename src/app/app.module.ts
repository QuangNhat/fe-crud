import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';

import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IndexComponent } from './modules/index/index.component';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { CreateComponent } from './modules/create/create.component';
import { RouterModule } from '@angular/router';
import { APP_ROUTES } from './app-routing.module';
import { UpdateComponent } from './modules/update/update.component';


@NgModule({
  declarations: [
    AppComponent,
    IndexComponent,
    CreateComponent,
    UpdateComponent
    
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MDBBootstrapModule.forRoot(),
    FormsModule,
    AngularFontAwesomeModule,
    ReactiveFormsModule,
    RouterModule.forRoot(APP_ROUTES, {
      scrollPositionRestoration: "enabled"
    }),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
