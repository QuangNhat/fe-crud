import { RouterModule, Routes } from "@angular/router";
import { IndexComponent } from "./modules/index/index.component";
import { CreateComponent } from "./modules/create/create.component";
import { UpdateComponent } from "./modules/update/update.component";

export const APP_ROUTES: Routes = [
    {
        path: "",
        redirectTo: "/",
        pathMatch: "full"
    },
    {
        path: "",
        component: IndexComponent
    },
    {
        path: "add",
        component: CreateComponent
    },
    {
        path: "update",
        component: UpdateComponent
    },
];